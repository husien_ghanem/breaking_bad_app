

import 'package:breaking_bad/constants/my_colors.dart';
import 'package:flutter/material.dart';

class InfoScreen extends StatelessWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: MyColors.myGrey),
        onPressed: () => Navigator.of(context).pop(),
      ),
      backgroundColor: MyColors.myYellow,
    ),
      body: Container(color: MyColors.myGrey,
      child: Center(
        child: Container(decoration: BoxDecoration(color:  MyColors.myYellow, borderRadius: BorderRadius.circular(15),)
           ,width: 200,height: 200,child:
          Column(children: [
            SizedBox(height: 30,),
            Text("directed by :",style:  TextStyle(color: MyColors.myGrey, fontSize: 18,decoration: null),) ,
            SizedBox(height: 30,),
            Text("Aracoders " ,style:   TextStyle(color: MyColors.myGrey, fontSize: 18),),
            SizedBox(height: 30,),
            Text("Husien Ghanem " ,style:   TextStyle(color: MyColors.myGrey, fontSize: 18,fontWeight: FontWeight.bold),)
            ,Text("0936219110" ,style:   TextStyle(color: MyColors.myGrey, fontSize: 18,fontWeight: FontWeight.bold),)

          ],),),
      ),),
    );
  }
}
